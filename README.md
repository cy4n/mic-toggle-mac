# toggle microphone on MacOS

Since MacOS does not offer a Shortcut to mute the microphone from the OS-Level (whatever app you might focus), i googled my way through fixing this.
(M$ Teams runs in either browser or electron and apparently MacOS shortcuts can not access functions inside this electron app apparently, and i think its absolutely horrible to focus the videochat app to mute yourself while doing something else, may it be presenting, hacking, pair programming, whatever)

the solution is to create an Automator "Quick Action" with the following apple-script, which basically just toggles the input level from 0 to 100 and vice versa:
```
if input volume of (get volume settings) = 0 then
	set level to 100
	say "microphone activated"
else
	set level to 0
	say "microphone muted"
end if
```
make sure you set the "Workflow receives" option to "no input"

you can obviously skip the `say` command or change the text, i prefer to get a notification when muting or unmuting my microphone.

![add quick action in automator](/img/automator_mictoggle.png){: .shadow}

after setting up (and saving!) the Automator action you can add a hotkey through Preferences -> Keyboard -> Shortcuts -> Service 
your new automator action will appear on the bottom by name of your quick action. 
(beware: MacOs is somehow broken for shortcuts and automator workflows. try add the command-key to your shortcut combination, apparently this fixes the issue for some ppl)

![add shortcut to run automator workflow](/img/shortcut.png){: .shadow}

you can also run your service from Finder -> Services -> <name>

![run service from finder](/img/finder_service.png){: .shadow}
